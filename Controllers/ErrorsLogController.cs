﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OLOS_test.Controllers
{
    public class ErrorsLogController : ApiController
    {
        [Route("AL/{idType}/{*id}")]
        public HttpResponseMessage GetByAnyID(string idType, string id)
        {
            if (id.Contains("/"))
            {
                id = id.Replace("/", "");
            }
            try
            {
                if (!idType.Equals("corrID") && !idType.Equals("reqID") && !idType.Equals("tripID"))
                {
                    throw new ArgumentException("No such ID Type exists.");
                }
                if (SQLCommands.GetByAnyID(idType, id).Rows.Count == 0)
                {
                    throw new ArgumentException("No data found with the given ID.");
                }
                return Request.CreateResponse(HttpStatusCode.OK, SQLCommands.GetByAnyID(idType, id));
            }
            catch (ArgumentException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [Route("callID")]
        public HttpResponseMessage getCallID()
        {
            return Request.CreateResponse(HttpStatusCode.OK, SQLCommands.callID());
        }

        [Route("ErrorByIDandDate/{date}/{IDType}/{*id}")]
        public HttpResponseMessage GetByDate(string date, string IDType, string id)
        {
            if (id.Contains("/"))
            {
                id = id.Replace("/", "");
            }
            try
            {
                if (SQLCommands.GetByIDandDate(date, IDType, id).Rows.Count == 0)
                {
                    throw new ArgumentException("No such record exists.");
                }
                return Request.CreateResponse(HttpStatusCode.OK, SQLCommands.GetByIDandDate(date, IDType, id));
            }
            catch (ArgumentException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [Route("ErrorByCaller/{callID}")]
        public HttpResponseMessage GetByCaller(string callID)
        {
            if (SQLCommands.GetByCaller(callID).Rows.Count == 0)
                {
                    throw new ArgumentException("No such record exists.");
                }
            return Request.CreateResponse(HttpStatusCode.OK, SQLCommands.GetByCaller(callID));
        }

        [Route("ErrorCountByCaller/{callID}")]
        public HttpResponseMessage GetCountByCaller(string callID)
        {
            if (SQLCommands.GetCountByCaller(callID).Rows.Count == 0)
            {
                throw new ArgumentException("No such record exists.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, SQLCommands.GetCountByCaller(callID));
        }

        [Route("EL/{idType}/{*id}")]
        public HttpResponseMessage GetErrorsByAnyID(string idType, string id)
        {
            if (id.Contains("/"))
            {
                id = id.Replace("/", "");
            }
            try
            {
                if (!idType.Equals("corrID") && !idType.Equals("reqID") && !idType.Equals("tripID"))
                {
                    throw new ArgumentException("No such ID Type exists.");
                }
                if (SQLCommands.GetByAnyID(idType, id).Rows.Count == 0)
                {
                    throw new ArgumentException("No data found with the given ID.");
                }
                return Request.CreateResponse(HttpStatusCode.OK, SQLCommands.GetErrorsByAnyID(idType, id));
            }
            catch (ArgumentException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }
    }
}

