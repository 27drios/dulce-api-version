﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace OLOS_test
{
    public class SQLCommands
    {
        //gets connection string from web.config
        public static string GetConnectionStringByName(string name)
        {
            string returnValue = null;
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];
            if (settings != null)
                returnValue = settings.ConnectionString;
            return returnValue;
        }

        public static DataTable GetByAnyID(string idType, string id)
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                connection.ConnectionString = GetConnectionStringByName("AuditLog");
                connection.Open();
                MySqlCommand myCommand = new MySqlCommand("stevenget_byall", connection);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.AddWithValue("IDType", idType);
                myCommand.Parameters.AddWithValue("ID", id);
                DataTable table = new DataTable("table");
                //var stringArr;
                using (var da = new MySqlDataAdapter(myCommand))
                {
                    da.Fill(table);
                    //stringArr = table.Rows[0].ItemArray.Select(x => x.ToString()).ToArray();
                }

                return table; // stringArr;
            }
        }

        public static Array callID()
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                connection.ConnectionString = GetConnectionStringByName("AuditLog");
                connection.Open();
                MySqlCommand myCommand = new MySqlCommand("dulce_callerid", connection);
                myCommand.CommandType = CommandType.StoredProcedure;
                DataTable table = new DataTable("table");
                using (var da = new MySqlDataAdapter(myCommand))
                {
                    da.Fill(table);
                }
                //return table;
                string[] strArray = new string[table.Rows.Count];
                int i = 0;
                foreach (DataRow dtRow in table.Rows)
                {
                    // On all tables' columns
                    foreach (DataColumn dc in table.Columns)
                    {
                        strArray[i] = dtRow[dc].ToString();
                    i++;
                    }
                }
                return strArray;
            }
        }

        public static DataTable GetByIDandDate(string date, string IDType, string ID)
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                DateTime dateConvert = DateTime.ParseExact(date, "yyyy-MM-dd-HH-mm-ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                connection.ConnectionString = GetConnectionStringByName("AuditLog");
                connection.Open();
                MySqlCommand myCommand = new MySqlCommand("stevenget_errorsbyidanddate", connection);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.AddWithValue("IDType", IDType);
                myCommand.Parameters.AddWithValue("ID", ID);
                myCommand.Parameters.AddWithValue("mytime", date);
                DataTable table = new DataTable("table");
                using (var da = new MySqlDataAdapter(myCommand))
                {
                    da.Fill(table);
                }
                return table;
            }
        }

        public static DataTable GetByCaller(string callID)
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                connection.ConnectionString = GetConnectionStringByName("AuditLog");
                connection.Open();
                MySqlCommand myCommand = new MySqlCommand("dulce_getdatacallerid", connection);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.AddWithValue("callID", callID);
                DataTable table = new DataTable("table");
                using (var da = new MySqlDataAdapter(myCommand))
                {
                    da.Fill(table);
                }
                return table;
            }
        }

        public static DataTable GetCountByCaller(string callID)
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                connection.ConnectionString = GetConnectionStringByName("AuditLog");
                connection.Open();
                MySqlCommand myCommand = new MySqlCommand("dulce_getcountcallerid", connection);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.AddWithValue("callID", callID);
                DataTable table = new DataTable("table");
                using (var da = new MySqlDataAdapter(myCommand))
                {
                    da.Fill(table);
                }
                return table;
            }
        }

        public static DataTable GetErrorsByAnyID(string idType, string id)
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                connection.ConnectionString = GetConnectionStringByName("AuditLog");
                connection.Open();
                MySqlCommand myCommand = new MySqlCommand("stevenget_errorsbyall", connection);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.AddWithValue("IDType", idType);
                myCommand.Parameters.AddWithValue("ID", id);
                DataTable table = new DataTable("table");
                //var stringArr;
                using (var da = new MySqlDataAdapter(myCommand))
                {
                    da.Fill(table);
                    //stringArr = table.Rows[0].ItemArray.Select(x => x.ToString()).ToArray();
                }

                return table; // stringArr;
            }
        }

    }
}